<?php

namespace Prolong\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * App
 *
 * @ORM\Table(name="Package")
 * @ORM\Entity(repositoryClass="Prolong\ParserBundle\Entity\Repository\PackageRepository")
 */
class Package
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string")
     */
    protected $description;

    /**
     * @var int
     *
     * @ORM\Column(name="installations", type="integer")
     */
    protected $installations;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getInstallations()
    {
        return $this->installations;
    }

    /**
     * @param int $installations
     *
     * @return $this
     */
    public function setInstallations($installations)
    {
        $this->installations = $installations;

        return $this;
    }
}
