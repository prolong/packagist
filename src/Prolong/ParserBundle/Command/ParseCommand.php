<?php

namespace Prolong\ParserBundle\Command;

use Guzzle\Http\Client;
use Guzzle\Http\Message\Request;
use Guzzle\Http\Message\Response;
use Prolong\ParserBundle\Entity\Package;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface as SymfonyInputInterface;
use Symfony\Component\Console\Output\OutputInterface as SymfonyOutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class ParseCommand extends ContainerAwareCommand
{
    /**
     * @param int $page
     *
     * @return string
     */
    public function parse($page)
    {
        $url = sprintf('https://packagist.org/search/?q=bundle&page=%d', $page);

        $client = new Client();

        /** @var Request $request */
        $request = $client->get($url);

        /** @var Response $response */
        $response = $request->send();

        /** @var string $data */

        return $response->getBody(true);
    }

    /**
     *
     */
    protected function configure()
    {
        $this->setName('parser:packagist');
    }


    protected function execute(SymfonyInputInterface $input, SymfonyOutputInterface $output)
    {
        $output->write('Start', true);

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $output->write('Parse page: 1', true);
        $data = $this->parse(1);

        $crawler = new Crawler();
        $crawler->addHtmlContent($data);

        $nav = $crawler->filter('nav a');

        $nodeValues = $crawler->filter('nav a')->each(function (Crawler $node, $i) {
            return $node->text();
        });

        $pages = $nodeValues[4];

        $page = 1;

        do {

            if ($page > 1) {
                $output->write('Parse page: '. $page, true);
                $data = $this->parse($page);

                $crawler = new Crawler();
                $crawler->addHtmlContent($data);
            }

            if ($crawler->filter('.packages li')->count()) {
                $packageList = $crawler->filter('.packages li')->each(function (Crawler $node, $i) {

                    $name = '';
                    if ($node->filter('a')->count()) {
                        $name = $node->filter('a')->text();
                    }

                    $description = '';
                    if ($node->filter('.package-description')->count()) {
                        $description = $node->filter('.package-description')->text();
                    }

                    $installations = 0;
                    if ($node->filter('.metadata')->count()) {
                        $metadata = $node->filter('.metadata')->text();
                        $metadata = trim($metadata);
                        $metadata = array_filter(explode('  ', $metadata));

                        foreach ($metadata as $key => $data) {
                            $data = str_replace(' ', '', $data);
                            $metadata[$key] = (int) $data;
                        }

                        $installations = $metadata[0];
                    }


                    $package = new Package();

                    $package->setName($name);
                    $package->setDescription($description);
                    $package->setInstallations($installations);

                    return $package;
                });


                foreach ($packageList as $package) {
                    $em->merge($package);
                }

                $em->flush();
            }
            $page++;

        } while ($page <= $pages);

        $output->write('Done', true);
    }
}
